<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RequestStoreTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testMiddlewareCoordinates()
    {
        $response = $this->get(route('stores'));
        $response->assertStatus(422);
    }

    public function testCoordinatesInvalid()
    {
        $response = $this->get(route('stores', ['longitude' => '-73.755404s', 'latitude' => '42.763655']));
        $response->assertStatus(422);
    }

    public function testApi()
    {
        $response = $this->get(route('stores', ['longitude' => '-73.755404', 'latitude' => '42.763655']));
        $response->assertStatus(200);
        return $response->getContent();
    }

    /**
     * @depends testApi
     */
    public function testJsonResponse($json)
    {
        $json = json_decode($json);
        $stores = count($json->data);
        $this->assertCount($stores, $json->data);
        return $json->data;
    }

    /**
     * @depends testJsonResponse
     */
    public function testDistance($stores)
    {
        if (count($stores) > 0) {
            $index = array_rand($stores, 1);
            $store = $stores[$index];
            $this->assertLessThanOrEqual(6.5, $store->distance);
        } else {
            $this->assertTrue(true);
        }
    }
}