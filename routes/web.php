<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([
    'namespace' => 'Api\V1',
    'prefix' => 'V1'
], function() {
    Route::get('stores', 'CompanyController@stores')->middleware(['coordinatis', 'logs'])->name('stores');
});
