<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class County extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public function city()
    {
        return $this->hasMany(City::class);
    }
}
