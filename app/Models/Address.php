<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'street_number',
        'street_name',
        'address_line_2',
        'address_line_3',
        'zip_code',
        'city_id',
        'location',
        'company_id',
    ];

    public $timestamps = false;

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
