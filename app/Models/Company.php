<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'license_number',
        'entity_name',
        'dba_name',
        'square_footage',
        'longitude',
        'latitude',
        'operation_type_id',
        'establishment_type_id',
    ];

    protected $casts = [
        'license_number' => 'integer',
        'square_footage' => 'integer',
        'longitude' => 'double',
        'latitude' => 'double',
    ];

    public function establishmentType()
    {
        return $this->belongsTo(EstablishmentType::class);
    }

    public function operationType()
    {
        return $this->belongsTo(OperationType::class);
    }

    public function address()
    {
        return $this->hasOne(Address::class);
    }
}
