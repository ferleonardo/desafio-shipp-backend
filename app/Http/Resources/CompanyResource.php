<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\AddressResource;

class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'license_number' => $this->license_number,
            'entity_name' => $this->entity_name,
            'dba_name' => $this->dba_name,
            'square_footage' => $this->square_footage,
            'operation_type' => $this->operationType,
            'establishment_type' => $this->establishmentType,
            'address' => new AddressResource($this->address),
            'longitude' => $this->longitude,
            'latitude' => $this->latitude,
            'distance' => (double) $this->distance,
        ];
    }
}
