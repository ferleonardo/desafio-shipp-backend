<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\CompanyService;
use App\Http\Resources\CompanyResource;

class CompanyController extends Controller
{
    protected $company;

    public function __construct(CompanyService $company)
    {
        $this->company = $company;
    }

    public function stores(Request $request)
    {
        $longitude = $request->get('longitude');
        $latitude = $request->get('latitude');

        $stores = $this->company->getAll($longitude, $latitude);
        return CompanyResource::collection($stores);
    }
}
