<?php

namespace App\Http\Middleware;

use Closure;
use Log;

class LogRequestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        $latitude = $request->get('latitude', null);
        $longitude = $request->get('longitude', null);
        $status_code = $response->getStatusCode();
        $total_stores = 0;

        if (isset($response->getData()->data)) {
            $total_stores = $response->getData()->meta->total;
        }

        $data = [
            'status_code' => $response->getStatusCode(),
            'latitude' => $latitude,
            'longitude' => $longitude,
            'total_stores' => $total_stores
        ];

        if ($response->getStatusCode() >= 400) {
            Log::channel('requests')->error(json_encode($data));
        } else {
            Log::channel('requests')->info(json_encode($data));
        }
    }
}
