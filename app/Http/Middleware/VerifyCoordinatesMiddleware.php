<?php

namespace App\Http\Middleware;

use Closure;

class VerifyCoordinatesMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (is_numeric($request->longitude) && is_numeric($request->latitude)) {
            return $next($request);
        }

        return response()->json("Parâmetros longitude e latitude mal formatados ou não existem.", 422);
    }
}
