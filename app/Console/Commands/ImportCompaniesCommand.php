<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Company;
use App\Models\Address;
use App\Models\County;
use App\Models\City;
use App\Models\State;
use App\Models\EstablishmentType;
use App\Models\OperationType;
use DB;

class ImportCompaniesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa empresas.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $option = $this->choice('Escolha uma opção:', [
            'Importar', 'Refazer importação'
        ]);

        if ($option == 'Refazer importação') {

            $this->comment('Este procedimento limpará todos os registros da tabela.');
            $check = $this->choice('Deseja continuar?', ['N', 'S']);
            
            if ($check == 'S') {
                DB::statement('PRAGMA foreign_keys=off;');
                DB::statement('DELETE FROM states;');
                DB::statement('DELETE FROM counties;');
                DB::statement('DELETE FROM cities;');
                DB::statement('DELETE FROM establishment_types;');
                DB::statement('DELETE FROM operation_types;');
                DB::statement('DELETE FROM addresses;');
                DB::statement('DELETE FROM companies;');
                DB::statement('DELETE FROM sqlite_sequence WHERE name = "states";');
                DB::statement('DELETE FROM sqlite_sequence WHERE name = "counties";');
                DB::statement('DELETE FROM sqlite_sequence WHERE name = "cities";');
                DB::statement('DELETE FROM sqlite_sequence WHERE name = "establishment_types";');
                DB::statement('DELETE FROM sqlite_sequence WHERE name = "operation_types";');
                DB::statement('DELETE FROM sqlite_sequence WHERE name = "addresses";');
                DB::statement('DELETE FROM sqlite_sequence WHERE name = "companies";');
                
                $this->info("Empresas deletadas com sucesso!");
                $this->line("Refazendo importação...");
                $this->import();
                DB::statement('PRAGMA foreign_keys=on;');
            } else {
                $this->line("Ação cancelada. Os registros da tabela companies não foram deletados.");
            }
        } else {
            $this->import();
        }
    }

    private function import()
    {
        $handle = fopen(storage_path('app/stores.csv'),'r');
        if ($handle !== false) {
            $i = 0;
            while (($data = fgetcsv($handle)) !== false) {
                if ($i > 0) {
                    if (Company::where('license_number', trim($data[1]))->first()) {
                        $this->error("Empresa {$data[1]} já foi importada.");
                        continue;
                    }

                    // Coordenadas
                    $latitude = null;
                    $longitude = null;
                    preg_match_all("/('(longitude|latitude)': '([0-9.-]+)')/", trim($data[14]), $matches);
                    if (count($matches[3]) == 2) {
                        $longitude = $matches[3][0];
                        $latitude = $matches[3][1];
                    }

                    // Empresa
                    $company = new Company();
                    $company->license_number = trim($data[1]);
                    $company->entity_name = trim($data[4]);
                    $company->dba_name = trim($data[5]);
                    $company->square_footage = trim($data[13]);
                    $company->longitude = $longitude;
                    $company->latitude = $latitude;
                    
                    // Tipos
                    $operationType = OperationType::firstOrCreate([
                        'name' => trim($data[2])
                    ]);

                    $establishmentType = EstablishmentType::firstOrCreate([
                        'name' => trim($data[3])
                    ]);

                    $company->operation_type_id = $operationType->id;
                    $company->establishment_type_id = $establishmentType->id;
                    $company->save();

                    // Município
                    $county = County::firstOrCreate([
                        'name' => trim($data[0])
                    ]);

                    // Estado
                    $state = State::firstOrCreate([
                        'acronym' => trim($data[11])
                    ]);

                    // Cidade
                    $city = City::firstOrCreate([
                        'name' => trim($data[10]),
                        'state_id' => $state->id,
                        'county_id' => $county->id
                    ]);

                    // Endereço
                    $addess = Address::create([
                        'street_number' => trim($data[6]),
                        'street_name' => trim($data[7]),
                        'address_line_2' => trim($data[8]),
                        'address_line_3' => trim($data[9]),
                        'zip_code' => trim($data[12]),
                        'location' => trim($data[14]),
                        'city_id' => $city->id,
                        'company_id' => $company->id
                    ]);

                    $this->info("Empresa {$company->license_number} importada com sucesso.");
                }
                $i++;
            }
        }
        fclose($handle);
    }
}
