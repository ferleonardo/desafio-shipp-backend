<?php

namespace App\Services;

use App\Repositories\CompanyRepository;

class CompanyService
{
    protected $repository;

    public function __construct(CompanyRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getAll($longitude, $latitude)
    {
        return $this->repository->getAll($longitude, $latitude);
    }
}