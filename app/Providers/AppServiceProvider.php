<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use File;
use DB;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Cria db sqlite, caso não exista
        if (config('database.default') === 'sqlite') {
            $db = config('database.connections.sqlite.database');
            File::exists($db) or File::put($db, '');
        }

        // Criando função sql
        DB::connection()->getPdo()->sqliteCreateFunction('DISTANCE', function () {
            if (count($geo = array_map('deg2rad', array_filter(func_get_args(), 'is_numeric'))) == 4) {
                return round(acos(sin($geo[0]) * sin($geo[2]) + cos($geo[0]) * cos($geo[2]) * cos($geo[1] - $geo[3])) * 6378.14, 3);
            }
            return null;
        }, 4);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
