<?php

namespace App\Repositories;

use App\Models\Company;
use DB;

class CompanyRepository
{
    protected $model;

    public function __construct(Company $model)
    {
        $this->model = $model;
    }

    public function getAll($longitude, $latitude)
    {
        return $this->model
                    ->selectRaw("*,  DISTANCE(latitude, longitude, {$latitude}, {$longitude}) as distance")
                    ->whereNotNull('latitude')
                    ->whereNotNull('longitude')
                    ->whereRaw("DISTANCE(latitude, longitude, {$latitude}, {$longitude}) <= 6.5")
                    ->orderByRaw("distance ASC")
                    ->paginate()
                    ->appends([
                        'longitude' => $longitude,
                        'latitude' => $latitude,
                    ]);
    }
}
