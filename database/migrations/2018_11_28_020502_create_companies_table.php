<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('establishment_type_id')->unsigned()->nullable();
            $table->integer('operation_type_id')->unsigned()->nullable();
            $table->integer('license_number');
            $table->string('entity_name')->index();
            $table->string('dba_name');
            $table->integer('square_footage');
            $table->integer('longitude')->nullable();
            $table->integer('latitude')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('establishment_type_id')->references('id')->on('establishment_types');
            $table->foreign('operation_type_id')->references('id')->on('operation_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
